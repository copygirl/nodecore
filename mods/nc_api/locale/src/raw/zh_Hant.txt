msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-17 04:43+0200\n"
"PO-Revision-Date: 2023-10-23 08:03+0000\n"
"Last-Translator: Yic95 <0Luke.Luke0@gmail.com>\n"
"Language-Team: Chinese (Traditional) <https://hosted.weblate.org/projects/"
"minetest/nodecore/zh_Hant/>\n"
"Language: zh_Hant\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 5.1.1-dev\n"

msgid "- Climbing spots also produce very faint light; raise display gamma to see."
msgstr "- 調高 gamma 值查看攀登點發出的微光。"

msgid "(C)2018-2021 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-2021，作者為 Aaron Suen <warr1024@@gmail.com>"

msgid "- @1"
msgstr "- @1"

msgid "- Aux+drop any item to drop everything."
msgstr "- 輔助鍵 + 丟任意物品丟掉所有物品。"

msgid "- Be wary of dark caves/chasms; you are responsible for getting yourself out."
msgstr "- 小心洞穴或深谷；你必須自己上來。"

msgid "- Can't dig trees or grass? Search for sticks in the canopy."
msgstr "- 無法砍樹或挖草？從樹冠尋找木棒。"

msgid "- Climbing spots may be climbed once black particles appear."
msgstr "- 出現黑色粒子即可爬上攀爬點。"

msgid "- Crafting is done by building recipes in-world."
msgstr "- 製作物品時，需在世界中建出配方。"

msgid "- DONE: @1"
msgstr "- 完成：@1"

msgid "- FUTURE: @1"
msgstr "- 延伸：@1"

msgid "- Hold/repeat right-click on walls/ceilings barehanded to climb."
msgstr "- 空手時，對牆或天花板按住／連點右鍵來攀爬。"

msgid "- If a recipe exists, you will see a special particle effect."
msgstr "- 如果配方存在，你會看到特殊粒子效果。"

msgid "- Learn to use the stars for long distance navigation."
msgstr "- 學會觀星來長途移動。"

msgid "- Recipes are time-based, punching faster does not speed up."
msgstr "- 配方以時間為基準，打更快不加快製作。"

msgid "- Sneak+aux+drop an item to drop all matching items."
msgstr "- 潛行時按輔助鍵丟物品會丟掉同樣的物品。"

msgid "- There is NO inventory screen."
msgstr "- 「沒有」物品欄畫面。"

msgid "- To run faster, walk/swim forward or climb/swim upward continuously."
msgstr "- 持續地向前／向上移動會愈來愈快。"

msgid "- Wielded item, target face, and surrounding nodes may matter."
msgstr "- 環繞的物品、面向與環繞的方塊可能影響。"

msgid "- You do not have to punch very fast (about 1 per second)."
msgstr "- 你不必打得很快（約一秒一次）。"

msgid "@1 ....."
msgstr "@1 ....."

msgid "@1 |...."
msgstr "@1 |...."

msgid "@1 ||..."
msgstr "@1 ||..."

msgid "@1 |||.."
msgstr "@1 |||.."

msgid "About"
msgstr "關於"

msgid "Active Lens"
msgstr "活化的透鏡"

msgid "Active Prism"
msgstr "活化的稜鏡"

msgid "Additional Mods Loaded: @1"
msgstr "已載入額外的模組： @1"

msgid "Adobe Mix"
msgstr "陶土"

#, fuzzy
msgid "Amalgamation"
msgstr "混合物"

msgid "Cobble"
msgstr "石"

msgid "Displaced Node"
msgstr "位移的方塊"

msgid "Eggcorn"
msgstr "種子"

msgid "Fire"
msgstr "火"

msgid "Gravel"
msgstr "礫石"

msgid "Growing Tree Trunk"
msgstr "樹幹生長點"

msgid "HAND OF POWER"
msgstr "力量之手"

msgid "Inventory"
msgstr "物品欄"

msgid "Leaves"
msgstr "樹葉"

msgid "Lens"
msgstr "透鏡"

msgid "Lit Torch"
msgstr "點燃的火把"

msgid "Log"
msgstr "原木"

msgid "Loose Cobble"
msgstr "鬆石"

msgid "Loose Sand"
msgstr "散沙"

msgid "Matrix: #nodecore:matrix.org"
msgstr "Matrix: #nodecore:matrix.org"

msgid "Molten Glass"
msgstr "熔融玻璃"

msgid "Movement"
msgstr "移動"

msgid "Stone Bricks"
msgstr "石磚塊"

msgid "Stone-Tipped Hatchet"
msgstr "石尖木短斧"

msgid "Stone-Tipped Mallet"
msgstr "石尖木槌"

msgid "Sprout"
msgstr "芽"

msgid "Sponge"
msgstr "海綿"

msgid "Stone-Tipped Pick"
msgstr "石尖木鎬"

msgid "Stump"
msgstr "根"

msgid "- Drop items onto ground to create stack nodes. They do not decay."
msgstr "- 在地面上丟物品來形成方塊堆。它們永不變質。"

msgid "- Do not use F5 debug info; it will mislead you!"
msgstr "- 不要使用 F5 除錯訊息；它會誤導你！"

msgid "- Drop and pick up items to rearrange your inventory."
msgstr "- 丟掉再拾起物品來重整物品欄。"

msgid "- "Furnaces" are not a thing; discover smelting with open flames."
msgstr "- 「熔爐」不是物品；用明火熔鍊。"

msgid "- For larger recipes, the center item is usually placed last."
msgstr "- 在較大的合成配方中，中心物通常最後放置。"

msgid "- Hopelessly stuck? Try asking the community chatrooms (About tab)."
msgstr "- 沒有進展？試著在社群聊天室發問（「關於」頁）。"

msgid "- Larger recipes are usually more symmetrical."
msgstr "- 較大的配方通常較為對稱。"

msgid "- Order and specific face of placement may matter for crafting."
msgstr "- 順序與放置面可能影響製作。"

msgid "- If it takes more than 5 seconds to dig, you don't have the right tool."
msgstr "- 挖掘超過 5 秒代表你使用了錯誤的工具。"

msgid "- Nodes dug without the right tool cannot be picked up, only displaced."
msgstr "- 錯誤的工具只能拾起位移的方塊。"

msgid "- Items picked up try to fit into the current selected slot first."
msgstr "- 拾起的物品會先嘗試進入選取的物品欄。"

msgid "- Displaced nodes can be climbed through like climbing spots."
msgstr "- 位移的方塊也可爬上。"

msgid "- Ores may be hidden, but revealed by subtle clues in terrain."
msgstr "- 礦石可能隱藏，地勢有微小線索。"

msgid "- Some recipes require "pummeling" a node."
msgstr "- 有些配方需要「敲打」方塊。"

msgid "- The game is challenging by design, sometimes frustrating. DON'T GIVE UP!"
msgstr "- 這個遊戲被刻意設計的很有挑戰性，有時令人挫折。不 要 放 棄 ！"

msgid "@1 ||||."
msgstr "@1 ||||."

msgid "- Sneak+drop to count out single items from stack."
msgstr "- 潛行時丟物品會從物品疊中丟出一個物品。"

msgid "- Stacks may be pummeled, exact item count may matter."
msgstr "- 堆疊可以被敲打，精確的數量可能影響。"

msgid "- Trouble lighting a fire? Try using longer sticks, more tinder."
msgstr "- 生火有困難？試著使用更長的木棒與更多的可燃物。"

msgid "- To pummel, punch a node repeatedly, WITHOUT digging."
msgstr "- 「敲打」是快速的打擊方塊，而非挖掘。"

msgid "- Tools used as ingredients must be in very good condition."
msgstr "- 被當成原料的工具必須接近全新。"

msgid "@1 |||||"
msgstr "@1 |||||"

msgid "@1 (@2)"
msgstr "@1 (@2)"

msgid "@1 discovered, @2 available, @3 future"
msgstr "@1 個發現、 @2 個可發現、 @3 個延伸"

msgid "Adobe Bricks"
msgstr "土胚磚塊"

msgid "Adobe"
msgstr "土胚方塊"

msgid "Aggregate"
msgstr "小石料"

msgid "Charcoal"
msgstr "木炭塊"

msgid "Ash"
msgstr "灰塊"

msgid "Charcoal Lump"
msgstr "木炭堆"

msgid "Stone Chip"
msgstr "石片"

msgid "Stone-Tipped Spade"
msgstr "石尖木鏟"

msgid "Ash Lump"
msgstr "灰堆"

msgid "CHEATS ENABLED"
msgstr "作弊模式"

msgid "Grass"
msgstr "草地"

msgid "Growing Leaves"
msgstr "樹葉生長點"

msgid "Living Sponge"
msgstr "活海綿"

msgid "Loose Leaves"
msgstr "落葉"

msgid "NodeCore"
msgstr "NodeCore"

msgid "Stone"
msgstr "岩"

msgid "Loose Dirt"
msgstr "散土"

msgid "Loose Gravel"
msgstr "散礫石"

msgid "MIT License (http://www.opensource.org/licenses/MIT)"
msgstr "MIT 授權條款 (http://www.opensource.org/licenses/MIT)"

msgid "Stick"
msgstr "木棒"

msgid "Staff"
msgstr "木棍"

msgid "Annealed Lode"
msgstr "冷卻類鋼"

msgid "Annealed Lode Adze"
msgstr "冷卻類鋼鋤"

msgid "Annealed Lode Hatchet"
msgstr "冷卻類鋼短斧"

msgid "Annealed Lode Mallet"
msgstr "冷卻類鋼鎚"

msgid "Annealed Lode Hatchet Head"
msgstr "冷卻類鋼短斧 頭"

msgid "Annealed Lode Mallet Head"
msgstr "冷卻類鋼鎚 頭"

msgid "Annealed Lode Pick"
msgstr "冷卻類鋼鎬"

msgid "Annealed Lode Spade"
msgstr "冷卻類鋼鏟"

msgid "Annealed Lode Spade Head"
msgstr "冷卻類鋼鏟 頭"

msgid "Annealed Lode Pick Head"
msgstr "冷卻類鋼鎬 頭"

msgid "Chromatic Glass"
msgstr "半透玻璃"

msgid "Clear Glass"
msgstr "透明玻璃"

msgid "Dirt"
msgstr "土"

msgid "Discovery"
msgstr "發現"

msgid "Lode Ore"
msgstr "類鋼礦"

msgid "Player's Guide: Crafting"
msgstr "玩家指南：製作"

msgid "Annealed Lode Rake"
msgstr "冷卻類鋼耙"

msgid "Crafting"
msgstr "製作"

msgid "DEVELOPMENT VERSION"
msgstr "開發版"

msgid "GitLab: https://gitlab.com/sztest/nodecore"
msgstr "GitLab: https://gitlab.com/sztest/nodecore"

msgid "Annealed Lode Prill"
msgstr "冷卻類鋼粒"

msgid "Lode Cobble"
msgstr "類鋼石"

msgid "Player's Guide: Movement and Navigation"
msgstr "玩家指南：移動與探索"

msgid "Player's Guide: Pummeling Recipes"
msgstr "玩家指南：敲打"

msgid "Player's Guide: Tips and Guidance"
msgstr "玩家指南：引導與訣竅"

msgid "NodeCore ALPHA"
msgstr "NodeCore ALPHA"

msgid "Player's Guide: Inventory Management"
msgstr "玩家指南：物品欄管理"

msgid "Sandstone"
msgstr "砂岩"

msgid "Sandstone Bricks"
msgstr "砂岩磚塊"

msgid "See included LICENSE file for full details and credits"
msgstr "查看內建的 LICENSE 檔案取得全部細節與 credits"

msgid "Torch"
msgstr "火把"

msgid "Tree Trunk"
msgstr "樹幹"

msgid "Loose Lode Cobble"
msgstr "鬆類鋼石"

msgid "Minetest's top original voxel game about emergent mechanics and exploration"
msgstr "最好的 Minetest 原創方塊遊戲，有關探索自然出現的遊戲機制"

msgid "Annealed Lode Frame"
msgstr "冷卻類鋼框"

msgid "Wooden Frame"
msgstr "木框"

msgid "assemble a wooden frame from staves"
msgstr "用一些木棍製作木框"

msgid "convert a wooden frame to a form"
msgstr "由木框製作成型框"

msgid "Crude Glass"
msgstr "粗糙玻璃"

msgid "Wooden Form"
msgstr "木成型框"

msgid "Wooden Mallet"
msgstr "木槌"

msgid "put a stone tip onto a wooden tool"
msgstr "在木製工具上放置一石片"

msgid "carve wooden tool heads from planks"
msgstr "把木材削成木工具頭"

msgid "breed a new flower variety"
msgstr "培育新種花朵"

msgid "assemble a rake from adzes and a staff"
msgstr "由木鋤與一木棒組裝木耙"

msgid "Wooden Pick Head"
msgstr "木鎬頭"

msgid "assemble a staff from sticks"
msgstr "由木棒組裝木棍"

msgid "Wooden Hatchet"
msgstr "木短斧"

msgid "assemble a wooden tool"
msgstr "組裝木製工具"

msgid "assemble an adze out of sticks"
msgstr "由木棒組裝木鋤"

msgid "Wooden Adze"
msgstr "木鋤"

msgid "Wooden Hatchet Head"
msgstr "木短斧 頭"

msgid "Wooden Ladder"
msgstr "木梯"

msgid "Wooden Mallet Head"
msgstr "木槌頭"

msgid "Wooden Pick"
msgstr "木鎬"

msgid "Wooden Plank"
msgstr "木材"

msgid "Wooden Rake"
msgstr "木耙"

msgid "Wooden Shelf"
msgstr "木儲物架"

msgid "Wooden Spade"
msgstr "木鏟"

msgid "Wooden Spade Head"
msgstr "木鏟頭"

msgid "assemble a wooden shelf from a form and plank"
msgstr "由木成型框與木材組成木儲物架"

msgid "assemble a wooden ladder from sticks"
msgstr "由木棒組裝木梯"

msgid "carve a wooden plank completely"
msgstr "完整地把木材削成木棒"

msgid "convert a wooden form to a frame"
msgstr "由木成型框製作木框"

msgid "temper a lode cube"
msgstr "調溫類鋼方塊"

msgid "split a tree trunk into planks"
msgstr "分原木為木材"

msgid "write on a surface with a charcoal lump"
msgstr "用木炭寫字"

msgid "squeeze out a sponge"
msgstr "擰海綿"

msgid "wet a concrete mix"
msgstr "沾溼混凝土粉"

msgid "Early-access edition of NodeCore with latest features (and maybe bugs)"
msgstr "有最新功能但可能出錯的 Nodecore"

msgid "stick a lens/prism in place"
msgstr "黏住透鏡或稜鏡"

msgid "temper a lode tool head"
msgstr "調溫一類鋼工具頭"

msgid "throw an item really fast"
msgstr "急速丟出物品"

msgid "Stone-Tipped Stylus"
msgstr "石尖刮刻筆"

msgid "cut down a tree"
msgstr "砍樹"

msgid "drop all your items at once"
msgstr "丟光身上的東西"

msgid "light a torch"
msgstr "點燃火把"

msgid "melt sand into molten glass"
msgstr "將沙融化成玻璃"

msgid "drop an item"
msgstr "丟東西"

msgid "change a stylus pattern"
msgstr "改變雕刻花紋"

msgid "dig up dirt"
msgstr "挖土"

msgid "pack high-quality charcoal"
msgstr "壓縮高品質碳"

msgid "Sand"
msgstr "沙"

msgid "Water"
msgstr "水"

msgid "Wet Sponge"
msgstr "溼海綿"

msgid "craft a torch from staff and coal lump"
msgstr "用木棍與碳製作火把"

msgid "dig leaves"
msgstr "挖樹葉"

msgid "dig up a tree stump"
msgstr "挖起樹根"

msgid "dig up cobble"
msgstr "挖石頭"

msgid "dig up lode ore"
msgstr "挖類鋼礦"

msgid "dig up gravel"
msgstr "挖礫石"

msgid "dig up sand"
msgstr "挖沙"

msgid "hold your breath"
msgstr "憋氣"

msgid "Annealed Lode Ladder"
msgstr "冷卻類鋼梯"

msgid "Tempered Lode Hatchet Head"
msgstr "調溫類鋼短斧 頭"

msgid "see a tree grow"
msgstr "看見樹成長"

msgid "- Something seems tedious? Find better tech, subtle factors, or a better way."
msgstr "- 遇到棘手問題時，發展科技，觀察微小變因，或尋找更好的方法。"

msgid "Annealed Lode Bar"
msgstr "冷卻類鋼條"

msgid "Annealed Lode Mattock"
msgstr "冷卻類鋼鶴嘴鋤"

msgid "Annealed Lode Mattock Head"
msgstr "冷卻類鋼鶴嘴鋤 頭"

msgid "Tempered Lode Hatchet"
msgstr "調溫類鋼短斧"

msgid "sinter glowing lode prills into a cube"
msgstr "鍛造紅熱的類鋼，使其成為方塊"

msgid "Glowing Lode Mallet Head"
msgstr "紅熱的類鋼鎚 頭"

msgid "Glowing Lode Hatchet Head"
msgstr "紅熱的類鋼短斧 頭"

msgid "assemble a stone-tipped stylus"
msgstr "組裝石尖刮刻筆"

msgid "Clear Glass Case"
msgstr "透明玻璃槽"

msgid "assemble a lode adze"
msgstr "組裝一支類鋼鋤"

msgid "assemble a lode rake"
msgstr "組裝一支類鋼耙"

msgid "break cobble into chips"
msgstr "打碎石頭，使它變石片"

msgid "anneal a lode cube"
msgstr "冷卻一個類鋼方塊"

msgid "assemble a clear glass case"
msgstr "組裝一個透明玻璃槽"

msgid "activate a lens"
msgstr "激發透鏡"

msgid "activate a prism"
msgstr "激發稜鏡"

msgid "Burning Embers"
msgstr "餘燼"

msgid "Humus"
msgstr "腐殖質"

msgid "Pummel"
msgstr "敲打"

msgid "Wet Adobe Mix"
msgstr "溼陶土"

msgid "Glowing Lode Pick Head"
msgstr "紅熱的類鋼鎬 頭"

msgid "Loose Humus"
msgstr "鬆腐殖質"

msgid "Tempered Lode"
msgstr "調溫類鋼"

msgid "The discovery system only alerts you to the existence of some basic game mechanics. More advanced content, such as emergent systems and automation, you will have to invent yourself!"
msgstr "發現提示系統只告訴你遊戲基本機制。更進階的內容，像警報與自動化系統，需要自己"
"發明！"

msgid "Glowing Lode Prill"
msgstr "紅熱的類鋼粒"

msgid "Glowing Lode Spade Head"
msgstr "紅熱的類鋼鏟 頭"

msgid "Peat"
msgstr "泥炭土"

msgid "Glowing Lode"
msgstr "紅熱的類鋼"

msgid "Tempered Lode Adze"
msgstr "調溫類鋼鋤"

msgid "Tempered Lode Mallet"
msgstr "調溫類鋼槌"

msgid "Tempered Lode Mallet Head"
msgstr "調溫類鋼槌 頭"

msgid "Tempered Lode Pick"
msgstr "調溫類鋼鎬"

msgid "Tempered Lode Prill"
msgstr "調溫類鋼粒"

msgid "find lode ore"
msgstr "找到類鋼礦"

msgid "work annealed lode on a tempered lode anvil"
msgstr "在調溫類鋼砧上敲打類鋼"

msgid "Glowing Lode Cobble"
msgstr "紅熱的類鋼石"

msgid "Tempered Lode Pick Head"
msgstr "調溫類鋼鎬 頭"

msgid "Glowing Lode Adze"
msgstr "紅熱的類鋼鋤"

msgid "Tempered Lode Spade Head"
msgstr "調溫類鋼鏟 頭"

msgid "Tempered Lode Rake"
msgstr "調溫類鋼耙"

msgid "Tempered Lode Spade"
msgstr "調溫類鋼鏟"

msgid "melt down lode metal from lode cobble"
msgstr "從類剛礦中熔煉出類鋼"

msgid "work glowing lode on a stone anvil"
msgstr "在石砧上敲打紅熱的類鋼"

msgid "work glowing lode on a lode anvil"
msgstr "在類鋼砧上敲打紅熱的類鋼"

msgid "Gated Prism"
msgstr "失效的棱鏡"

msgid "Glowing Lode Mattock Head"
msgstr "紅熱的類鋼槌 頭"

msgid "Glued Active Lens"
msgstr "上膠的活化透鏡"

msgid "Glued Active Prism"
msgstr "上膠的活化棱鏡"

msgid "Glowing Lode Rake"
msgstr "紅熱的類鋼耙"

msgid "Glued Prism"
msgstr "上膠的棱鏡"

msgid "plant an eggcorn"
msgstr "種下種子"

msgid "Glued Lens"
msgstr "上膠的透鏡"

msgid "Tips"
msgstr "訣竅"

msgid "find charcoal"
msgstr "發現木炭"

msgid "find dry (loose) leaves"
msgstr "發現落葉"

msgid "Glowing Lode Frame"
msgstr "紅熱的類鋼框"

msgid "Glued Shining Lens"
msgstr "上膠的發光透鏡"

msgid "Prism"
msgstr "稜鏡"

msgid "Tempered Lode Ladder"
msgstr "調溫類鋼梯"

msgid "Tempered Lode Mattock Head"
msgstr "調溫類鋼鶴嘴鋤 頭"

msgid "Annealed Lode Rod"
msgstr "冷卻類鋼棒"

msgid "Glowing Lode Bar"
msgstr "紅熱的類鋼條"

msgid "Glued Gated Prism"
msgstr "上膠的失效棱鏡"

msgid "IRC: #nodecore @@ irc.libera.chat"
msgstr "IRC: #nodecore @@ irc.libera.chat"

msgid "Shining Lens"
msgstr "發光棱鏡"

msgid "Tempered Lode Bar"
msgstr "調溫類鋼條"

msgid "Tempered Lode Frame"
msgstr "調溫類鋼框"

msgid "Tempered Lode Mattock"
msgstr "調溫類鋼鶴嘴鋤"

msgid "Tempered Lode Rod"
msgstr "調溫類鋼棒"

msgid "find a stick"
msgstr "發現木棒"

msgid "find an eggcorn"
msgstr "發現種子"

msgid "find ash"
msgstr "發現灰"

msgid "gate a prism"
msgstr "使棱鏡失效"

msgid "go for a swim"
msgstr "外出游泳"

msgid "https://content.minetest.net/packages/Warr1024/nodecore/"
msgstr "https://content.minetest.net/packages/Warr1024/nodecore/"

msgid "harvest a sponge"
msgstr "採收海綿"

msgid "Glowing Lode Ladder"
msgstr "紅熱的類鋼梯"

msgid "Glowing Lode Rod"
msgstr "紅熱的類鋼棒"

msgid "- Hold/repeat right-click on walls/ceilings barehanded to create climbing spots."
msgstr "要創造攀爬點，空手對牆或天花板按住右鍵。"

msgid "Discord: https://discord.gg/NNYeF6f"
msgstr "Discord: https://discord.gg/NNYeF6f"

msgid "Admin Tool"
msgstr "管理者工具"

msgid "Lode Form"
msgstr "類鋼成形框"

msgid "run at full speed"
msgstr "全速衝刺"

msgid "Raked Gravel"
msgstr "耙過的礫石"

msgid "Raked Humus"
msgstr "耙過的腐殖質"

msgid "weaken stone by soaking"
msgstr "泡水弱化石頭"

msgid "weld glowing lode pick and spade heads together"
msgstr "將紅熱的的鎬頭跟鏟頭接在一起"

msgid "rake humus"
msgstr "耙腐殖質"

msgid "rake gravel"
msgstr "耙礫石"

msgid "Loose Amalgamation"
msgstr "鬆混合物"

msgid "Raked Dirt"
msgstr "耙過的土"

msgid "Raked Sand"
msgstr "耙過的沙"

msgid "ferment peat into humus"
msgstr "把泥炭土發酵成腐殖質"

msgid "pack stone chips back into cobble"
msgstr "把一些石片敲打成石頭"

msgid "navigate by touch in darkness"
msgstr "黑暗中摸物前進"

msgid "rake dirt"
msgstr "耙土"

msgid "NodeCore updated from @1 to @2"
msgstr "NodeCore 已從 @1 更新至 @2"

msgid "rake sand"
msgstr "耙沙"

msgid "produce light from a lens"
msgstr "用透鏡製造光明"

msgid "mold molten glass into clear glass"
msgstr "把熔融玻璃塑造成透明玻璃"

msgid "find a sedge"
msgstr "找到莎草"

msgid "Sedge"
msgstr "莎草"

msgid "Thatch"
msgstr "草蓆"

msgid "pack sedges into thatch"
msgstr "把莎草敲打成草蓆"

msgid "grow a sedge on grass near moisture"
msgstr "在草地或潮濕地培養莎草"

msgid "(C)2018-@1 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-@1，作者為 Aaron Suen <warr1024@@gmail.com>"

msgid "Donate: https://liberapay.com/NodeCore"
msgstr "Donate: https://liberapay.com/NodeCore"

msgid "Offline @1 year(s)"
msgstr "已下線 @1 年"

msgid "Offline @1 hour(s)"
msgstr "已下線 @1 小時"

msgid "Offline @1 week(s)"
msgstr "已下線 @1 周"

msgid "Offline @1 minute(s)"
msgstr "已下線 @1 分鐘"

msgid "Offline @1 day(s)"
msgstr "已下線 @1 天"
