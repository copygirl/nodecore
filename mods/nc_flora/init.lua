-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("rushes")
include("sedges")
include("flowers")
include("thatch")
include("wicker")
include("cheat")
include("hints")
