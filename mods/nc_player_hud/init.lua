-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("setup")
include("breath")
include("hotbar")
include("touchtip")
include("looktip")
include("wieldtip")
include("crosshair")
include("pretrans")
include("cheats")
include("hints")
